BUILD = build
BOOKNAME = LaSantísimaTrinidad
TITLE = title.txt
METADATA = metadata.xml

CHAPTERS = 000-Advertencia.md 001-Introducción.md \
					 002-Capítulo1.md \
					 003-Capítulo.md \
					 004-Capítulo.md \
					 005-Capítulo.md \
					 006-Capítulo.md \
					 007-Capítulo.md \
					 008-Capítulo.md \
					 009-Capítulo.md \
					 010-Capítulo.md \
					 011-Conclusión.md \
					 012-Glosario.md
PREFIX_FOLDER = chp

TOC = --toc --toc-depth=2
COVER_IMAGE = img/cover.png
LATEX_CLASS = report
KINDLEC = ~/KindleGen/kindlegen

all: book

book: epub html pdf

clean:
	rm -r $(BUILD)

epub: $(BUILD)/epub/$(BOOKNAME).epub

html: $(BUILD)/html/$(BOOKNAME).html

pdf: $(BUILD)/pdf/$(BOOKNAME).pdf

mobi: epub $(BOOKNAME).mobi

$(BUILD)/epub/$(BOOKNAME).epub: $(TITLE) $(addprefix $(PREFIX_FOLDER)/, $(CHAPTERS))
	mkdir -p $(BUILD)/epub
	pandoc $(TOC) -f markdown+smart --epub-metadata=$(METADATA) --epub-cover-image=$(COVER_IMAGE) -o $@ $^

$(BUILD)/html/$(BOOKNAME).html: $(addprefix $(PREFIX_FOLDER)/, $(CHAPTERS))
	mkdir -p $(BUILD)/html
	pandoc $(TOC) --standalone --to=html5 -o $@ $^

$(BUILD)/pdf/$(BOOKNAME).pdf: $(TITLE) $(addprefix $(PREFIX_FOLDER)/, $(CHAPTERS))
	mkdir -p $(BUILD)/pdf
	pandoc $(TOC) --pdf-engine=xelatex -V documentclass=$(LATEX_CLASS) -o $@ $^

$(BOOKNAME).mobi: $(BUILD)/epub/$(BOOKNAME).epub
	mkdir -p $(BUILD)/mobi
	$(KINDLEC) -o $@ $^
#	mv $(BUILD)/epub/$(BOOKNAME).mobi $(BUILD)/mobi/


.PHONY: all book clean epub html pdf
