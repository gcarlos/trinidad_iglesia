# GLOSARIO

**Palabras técnicas y afines de la reflexión trinitaria**



ACCIÓN AD EXTRA (hacia fuera): Se dice de las acciones que la Trinidad realiza hacia fuera del círculo trinitario, como la creación del universo, la revelación, la salvación de los seres humanos.

ACCIÓN AD INTRA (hacia dentro): Se dice de las acciones intratrinitarias, dentro del círculo trinitario, como la generación del Hijo y la espiración del Espíritu Santo.

ACCIÓN APROPIADA: Es una acción atribuida a una de las personas divinas, aunque sea realizada juntamente por las tres, debido a una afinidad con las propiedades de aquella persona. Así, se atribuye al Padre la creación, al Hijo la redención y al Espíritu Santo la santificación.

ACCIÓN PROPIA: Es una acción específica de una persona determinada, como la encarnación del Hijo o la venida del Espíritu Santo sobre María en el momento de la concepción de Jesús.

AFIRMACIÓN ESENCIAL: Es aquella afirmación que se fundamenta en la esencia divina, igual y única en las tres personas. Una afirmación esencial es, por ejemplo, decir que Dios es misericordioso, infinito, eterno; es decir: la esencia divina es eterna, infinita, misericordiosa.

AFIRMACIÓN NOCIONAL: Es aquella que se basa solamente en las personas en su distinción unas de otras. Hay cuatro afirmaciones nocionales: el Padre engendra, el Hijo es engendrado, el Padre y el Hijo (o el Padre por el Hijo) espiran al Espíritu Santo, el Espíritu Santo es espirado por el Padre y por el Hijo (o a través del Hijo).

ANÁFORA: Literalmente significa "ofrecimiento"; es la parte central de la celebración eucarística, que incluye la consagración, la anamnesis (recuerdo de la pasión, muerte, resurrección y ascensión de Cristo) y la comunión.

ANAMNESIS: Literalmente significa "memorial"; es el recuerdo, después de la consagración del pan y del vino, de la pasión, muerte, resurrección y ascensión de Cristo.

APOFÁTICO: Literalmente significa "sin palabra"; es la actitud del teólogo ante el misterio divino; después de decir todo lo que puede, guarda silencio respetuosamente. Se dice que hay una teología apofática, que termina en el silencio de la veneración y la adoración.

ARKÉ: Expresión griega para significar el hecho de que el Padre es principio, fuente y causa única en la generación del Hijo y en la espiración del Espíritu Santo. Véase principio, causa.

ARRIANISMO: Es una herejía propuesta por Arrio (250-336), sacerdote de Alejandría (Egipto). Arrio afirmaba el subordinacionismo, o sea: el Hijo (y el Espíritu Santo) son subordinados al Padre; son criaturas sublimes, creadas antes del universo, pero no son Dios. Está, además, el subordinacionismo adopcionista: el Hijo fue adoptado como Hijo por gracia del Padre, pero no tiene la misma naturaleza del Padre.

CARISMA: En griego significa "gracia"; es un don o una habilidad que el Espíritu Santo concede a una persona con vistas al bien dé todos.

CIRCUMINCESIÓN: Significa la interpenetración activa de las personas divinas entre sí, debido a la comunión eterna que vige entre ellas. Véase Perijóresis.

CIRCUMINSESIÓN: Indica el estar o el morar de una persona en otra, ya que cada persona divina solamente existe en la otra, con la otra, por la otra y para la otra. Véase Perijóresis.

DOXOLOGÍA: Fórmula de alabanza (doxa en griego). Aparece generalmente al final de las oraciones, en las que se da gracias al Padre por el Hijo en la unidad del Espíritu Santo.

DS: Abreviación del nombre de dos teólogos (Denzinger-Schónmetzer), que publicaron el libro Enchiridion Symbolorum, de f initionum et declarationum de rebus fidei et morum, que es un elenco de los credos, definiciones y declaraciones sobre asuntos de fe y de moral que el magisterio de la Iglesia (concilios, sínodos y pronunciamientos oficiales del papa) pronunció a lo largo de la historia del cristianismo. La primera edición es de 1854, y la última (32), de 1963.

ECONOMÍA: Son las diversas fases de realización del proyecto de Dios en la historia o de la progresiva revelación del mismo Dios; en el campo trinitario, economía significa el orden en la procesión a partir del Padre: en primer lugar viene el Hijo, y luego el Espíritu Santo.

EK: Partícula griega que corresponde al latín ex o de, y significa la procedencia de una persona divina de la otra. Así, el Hijo es engendrado de (ek o ex o de) el Padre; el Espíritu Santo procede del Padre y del Hijo (según la teología latina).

EKPOREUSIS: Término griego para designar la procedencia del Espíritu Santo a partir del Padre, que es siempre Padre del Hijo. En latín, el término es spiratio (espiración).

        EPIKLESIS: Celebración en la que se invoca al Espíritu Santo.

ESENCIA DIVINA: Es aquello que constituye al Dios trino en sí mismo, la divinidad; es el ser, el amor, la bondad, la verdad y la comunión recíproca, en la forma de lo absoluto e infinito. Véanse también Naturaleza, Sustancia.

ESPIRACIÓN: Acto por el que el Padre, junto con el Hijo, hace proceder a la persona del Espíritu Santo (según los latinos) como de un único principio. Los griegos hacen proceder al Espíritu solamente del Padre y del Hijo o del Padre a través del Hijo.

FILIOQUE: Literalmente, "y del Hijo"; doctrina según la cual el Espíritu Santo procede del Padre y del Hijo como de un solo principio. Esta interpretación doctrinal se llama también "filioquismo"; es frecuente entre los teólogos latinos.

GENNESIS: Término griego para expresar la generación del Hijo por parte del Padre.

GESTALT RELACIONAL: Término usado por el teólogo alemán J. Moltmann para expresar la contribución del Hijo en la espiración del Espíritu Santo junto con el Padre; la persona del Espíritu proviene del Padre, mientras que la configuración concreta (Gestalt) de la persona del Espíritu Santo se deriva del Hijo. Es relacional, porque las personas están siempre vueltas hacia las otras y dentro de las otras.

HOMOIOUSIOS: Literalmente, "de naturaleza semejante"; herejía según la cual el Hijo no es igual, sino de naturaleza semejante al Padre.

HOMOIOUSIOS: Literalmente, "de la misma o igual naturaleza"; se dice que el Hijo y el Espíritu Santo tienen la misma e igual naturaleza que el Padre; las personas son consustanciales.

HIPÓSTASIS: Término griego para designar a la persona divina. Véanse Persona y Prósopon.

INNASCIBILIDAD: Propiedad exclusiva del Padre, la de no ser engendrado ni derivado de nadie; es principio sin principio.

KÉNOSIS: Expresión griega que significa "aniquilamiento" o "vaciamiento"; es el modo que escogieron las personas divinas (el Hijo y el Espíritu Santo) de comunicarse en la historia. Se opone a doxa, que significa el modo glorioso.

KOINONÍA: Expresión griega, equivalente a communio (comunión) en latín; es el modo propio de relacionarse entre sí las personas, incluso las divinas.

MISIÓN: En la teología trinitaria significa la autocomunicación de la persona del Hijo a la naturaleza humana de Jesús de Nazaret, y del Espíritu Santo a los justos, a María y a la Iglesia. Se trata de la entronización de la humanidad en el seno del misterio trinitario.

MISTERIO: En sentido estricto significa la realidad de la santísima Trinidad como inaccesible a la razón humana; incluso después de comunicada, puede ser conocida indefinidamente sin ser captada jamás totalmente por la mente humana. Dios trino es misterio en sí mismo, no sólo para la mente humana, ya que la Trinidad es esencialmente infinita y eterna. En sentido histórico-salvífico, el Dios trino es un misterio sacramental, o sea, un misterio que nos es comunicado por las actitudes y palabras de Jesús y en la acción del Espíritu Santo en la comunidad eclesial y en la historia humana.

MODALISMO: Doctrina herética según la cual la Trinidad constituye sólo tres modos de ver humanos del único y mismo Dios, o también tres modos (máscaras) de manifestarse el mismo y único Dios a los seres humanos; Dios no sería trinidad en sí, sino estrictamente uno y único.

MONARQUÍA: En lenguaje trinitario significa la causalidad única del Padre; sólo el Padre engendra al Hijo y espira, siendo Padre del Hijo, al Espíritu Santo; es una expresión típica de la teología greco-ortodoxa.

MONARQUIANISMO: Es la negación de la Trinidad en nombre de un estricto monoteísmo.

MONOTEÍSMO: Es la afirmación de la existencia de un uno y único Dios; el Antiguo Testamento conoce un monoteísmo pre-trinitario, anterior a la revelación de la santísima Trinidad; puede haber, después de la revelación del misterio de la Trinidad, un monoteísmo atrinitario: se habla de Dios sin tener en cuenta la trinidad de personas, como si Dios fuera una realidad única y existiera sólo en su sustancia; existe el monoteísmo trinitario: Dios es uno y único, debido a la única sustancia que existe en el Padre, en el Hijo y en el Espíritu Santo, o debido a la comunión eterna y a la perijóresis que vige desde el principio entre las tres divinas personas.

NATURALEZA DIVINA: Es la sustancia divina una y única en cada una de las personas; responde a la unidad o a la unión en Dios.

NOCIÓN: Son las características propias de cada una de las personas, que las diferencian a unas de otras: la paternidad y la innascibilidad para el Padre, la filiación para el Hijo, la espiración activa para el Padre y el Hijo, la espiración pasiva para el Espíritu Santo. Por tanto, hay cinco nociones.

PATREQUE: Literalmente, "y por el Padre"; en la Trinidad todas las relaciones son ternarias; así el Hijo se relaciona con el Espíritu Santo junto con el Padre o por el Padre; de la misma forma, el Espíritu Santo ama al Hijo por el Padre y junto con el Padre, etc.

PEGHE: Expresión griega para designar al Padre como fuente única e infinita de donde brotan el Hijo y el Espíritu Santo.

PERIJÓRESIS: Expresión griega que significa literalmente que una persona contiene a las otras dos (sentido estático) o que cada una de las personas interpenetra a las otras, y recíprocamente (sentido activo). El adjetivo perijorético designa el carácter de comunión que vige entre las divinas personas. Véanse Circumincesión y Circuminsesión.

PERSONA: En lenguaje trinitario significa lo que es distinto en Dios; es la individualidad de cada persona, que existe simultáneamente en sí y para sí y en eterna comunión con las otras dos. Véanse Hipóstasis y Subsistencia.

PROCESIÓN: Es la derivación de una persona a partir de la otra, pero consustancialmente, en la unidad de una misma naturaleza, sustancia, esencia o divinidad.

PRÓSOPON: Literalmente significa máscara o careta; en lenguaje trinitario es una palabra griega para designar a la persona divina en su individualidad; es sinónimo de hipóstasis. Véase Persona.

RELACIÓN: En lenguaje trinitario significa la ordenación de una persona a las otras, o la eterna comunión entre los divinos tres. Hay cuatro relaciones: paternidad, filiación, espiración activa y espiración pasiva.

SABELIANISMO: Herejía de Sabelio (comienzos del siglo II en Roma), llamada también modalismo: el Hijo y el Espíritu Santo serían simples modos de manifestación de la divinidad, y no personas distintas. Véase Modalismo.

SÍMBOLO: En sentido técnico de la teología antigua designa los formularios por los que la Iglesia resumía oficialmente su fe; es sinónimo de credo.

SPIRITUQUE: Literalmente, "y del Espíritu"; como las relaciones en la Trinidad son siempre ternarias, se dice que el Padre engendra el Hijo junto con el Espíritu Santo, o que el Hijo reconoce al Padre junto con el Espíritu Santo.

SUBORDINACIONISMO: Es la herejía de Arrio, según la cual el Hijo y el Espíritu Santo estarían subordinados, en relación desigual, al Padre, sin poseer de forma idéntica la misma naturaleza; serían entonces criaturas excelentes, sólo adoptadas (adopcionismo) por el Padre en su divinidad.

SUBSISTENCIA: Es uno de los sinónimos de persona o hipóstasis; como en la Trinidad no hay nada accidental, se dice que las relaciones entre las personas son relaciones subsistentes; la persona es considerada como una relación subsistente.

SUSTANCIA: En lenguaje trinitario designa lo que une en Dios y es idéntico en cada una de las personas. Véase Naturaleza y Esencia.

TEOGONÍA: Proceso por el que surge la divinidad o explicación del misterio de la Trinidad de tal forma que da la impresión de que las personas no son coeternas y coiguales, sino que se producen unas a otras.

TEOLOGÍA: En lenguaje trinitario designa la Trinidad en sí misma, prescindiendo de su manifestación en la historia; teología se opone entonces a economía.

TEOLOGÚMENO: Se dice de una teoría teológica propuesta por los teólogos, pero que no pertenece al depósito de la fe; es un teologúmeno afirmar, por ejemplo, que el Espíritu Santo asumió la realidad humana de María, haciendo de su maternidad humana una maternidad divina.

TRIADA (TRIAS): Expresión griega para designar la trinidad de personas.

TRINIDAD ECONÓMICA: Es la Trinidad en cuanto que se autorreveló en la historia de la humanidad y actúa con vistas a nuestra participación en la comunidad trinitaria.

TRINIDAD INMANENTE: Es la Trinidad considerada en sí misma, en su eternidad y comunión perijorética entre el Padre, el Hijo y el Espíritu Santo.
