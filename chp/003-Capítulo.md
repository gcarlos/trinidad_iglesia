# El proceso de revelación de la santísima Trinidad


## 10. ¿Cómo se reveló el Padre de cariño infinito?

El texto más importante que se aduce para la revelación de la santísima Trinidad por parte de Jesús es su palabra de despedida en Mateo: "Id, pues, y haced discípulos míos en todos los pueblos, bautizándolos en el nombre del Padre y del Hijo y del Espíritu Santo" (28,19). Este mandato de Jesús sólo se encuentra en el evangelio de san Mateo; falta en los otros tres evangelios.

Los estudiosos piensan que esta fórmula es tardía, ya que recoge la experiencia bautismal de la comunidad primitiva en el tiempo en que se escribió el evangelio de san Mateo, por el año 85. Aquella comunidad había meditado mucho en la vida y en las palabras de Jesús. A partir de allí comprendió que Jesús nos había revelado de hecho quién es Dios, es decir, la santísima Trinidad, y que en nombre de ese Dios trino tenían que ser bautizados los creyentes. Jesús está en el origen de esta fórmula eclesial.

Vamos a considerar cómo nos reveló Jesús las tres personas divinas. Comencemos por el nombre del Padre. Sabemos que Jesús siempre llamó a Dios Abba, que quiere decir "papá". Si uno llama a Dios Padre es porque se siente hijo. Este Padre es de infinita bondad y misericordia. Jesús mantuvo en sus largas oraciones una profunda intimidad con él. Si se muestra misericordioso con los pecadores es porque está imitando al Padre celestial, que es fundamentalmente misericordioso y ama a los ingratos y malos (Lc 6,35).

¿Cómo actúa el Padre? El Padre actúa en el mundo con vistas a la implantación de su Reino. Jesús hace del mensaje del reino de Dios el centro de su predicación. Reino no significa un territorio sobre el cual tiene dominio un rey. Reino es el modo de actuar del Padre mediante el cual va liberando a toda la creación de los males del pecado, de la enfermedad, de las divisiones y de la muerte, e implantando el amor, la fraternidad y la vida.

Jesús, con su palabra y con su práctica, se empeña en inaugurar ya en este mundo el reino del Padre. Y lo hace, como veremos a continuación, en la fuerza del Espíritu Santo. Jesús se siente tan unido con este Padre, que puede confesar: "Yo y el Padre somos una sola cosa" (Jn 10,30). El Padre amó al Hijo "antes de la creación del mundo" (Jn 17,24). Por tanto, incluso antes de ser creador, Dios era el Padre del Hijo eterno, que se encarnó y se llamó Jesucristo. El nos revela al Padre porque dijo: "El que me ha visto a mí ha visto al Padre" (Jn 14,9).

>El Padre es Padre, no ante todo por ser creador. Antes de la creación ya era Padre, porque eternamente era el Padre del Hijo. En el Hijo él nos imaginó como hijos e hijas suyos y, por tanto, como hermanos y hermanas del Hijo. Desde siempre estábamos en el corazón del Padre. Allí están nuestras raíces.


## 11. ¿Cómo se reveló el Hijo, nuestro hermano?

El Hijo se reveló asumiendo la santa humanidad de Jesús de Nazaret. Pero debemos respetar el camino que él escogió para manifestarse a las personas. No empezó diciendo enseguida que estaba encarnado en Jesús. Los discípulos, viendo cómo rezaba, cómo actuaba y cómo hablaba, fueron descubriendo la realidad de la filiación divina de Jesús, y así descubrieron la presencia de la segunda persona de la santísima Trinidad.

En primer lugar, el Hijo se revela en la forma de rezar de Jesús. Llama a Dios su "querido papá". El que llama a Dios papá se siente su hijo querido. Y, de hecho, Jesús dice: "Nadie conoce al Padre sino el Hijo y aquel a quien el Hijo se lo quiera manifestar" (Lc 10,22). En la oración Jesús revelaba su unión e intimidad con el Padre. Entonces podía decir: "Yo y el Padre somos una sola cosa" (Jn 10,30). Se sentía Hijo, pero con la misma naturaleza del Padre, viviendo una misma comunión.

En segundo lugar, Jesús actuaba como quien era el Hijo de Dios y el representante del Padre. Se compadecía de todos los que sufrían y de todos los pobres. Curaba y consolaba. Las personas agraciadas tenían la sensación de estar ante el poder personalizado de Dios. Pedro confesaba: "¡Tú eres el Hijo de Dios vivo!" Los enemigos de Jesús se dieron cuenta de que Jesús invadía el espacio divino. Perdonaba pecados, cosa que solamente Dios puede hacer; modificaba la ley santa del Antiguo Testamento o introducía interpretaciones liberadoras. Con razón le acusaban: "Llama a Dios su propio Padre, haciéndose igual a Dios" (Jn 5,18).

En tercer lugar, el mismo cielo dio testimonio en favor de Jesús, el Hijo de Dios. No sabemos si el relato bíblico se refiere a un acontecimiento concreto o se trata de expresar, por esta forma literaria, la experiencia íntima de Jesús, comunicada de alguna manera a los discípulos. De todas formas, en el bautismo de Jesús y en la transfiguración del monte Tabor se oyó la voz: "Este es mi Hijo amado, mi predilecto" (Mt 3,17; 17,5). Aquí se revela lo que Jesús escondía con recato: su filiación divina.

Finalmente, la muerte y la resurrección de Jesús son momentos cruciales en los que se revela la verdadera naturaleza de Dios y de las otras dos personas divinas: el amor y la plena comunión. En la muerte, Jesús entrega totalmente su vida a los demás. Esta muerte es fruto del rechazo que Jesús sufrió. Pero no deja que la muerte sea solamente expresión del rechazo de su persona, del Dios que anuncia y del Reino. Asume libremente la muerte como expresión suprema de su amor para con quien lo rechaza. Quiere que la última palabra la tenga la comunión y no la exclusión. Jesús murió en solidaridad y en comunión hasta con los enemigos que le condenaban para garantizar el triunfo del amor y de la comunión. Este triunfo se revela en la resurrección, que es la plenitud de la vida en total comunicación y realización. Esta vida revelada en la resurrección es la misma que estaba en la cruz. Por eso existe una unidad entre la muerte y la resurrección: hay un solo misterio pascual. Este misterio revela la esencia de la santísima Trinidad: el amor y la comunión. En este misterio está presente el Padre, que ama y que sufre con el Hijo; está presente el Espíritu Santo, por cuya fuerza el Hijo entrega su vida y mantiene la comunión hasta el fin.

> Si queremos estar unidos a la santísima Trinidad, hemos de seguir el mismo camino que Jesús: rezar con intimidad, actuar con radicalidad por la justicia y la comunión y aceptar la misma muerte como forma de entrega total y de comunión última hasta con los enemigos.



## 12. ¿Cómo se reveló el Espíritu Santo, nuestra fuerza?

El Espíritu Santo es la segunda mano por la que el Padre nos alcanza y nos abraza. El Padre y el Hijo enviaron al mundo al Espíritu Santo. Ya antes el Espíritu actuaba desde siempre en la tierra: fomentando la vida, animando el coraje de los profetas, inspirando sabiduría para las acciones humanas. Su mayor obra fue venir sobre María y formar en su seno la santa humanidad del Hijo encarnado en Jesús; bajó sobre Jesús con ocasión del bautismo de Juan; en la fuerza del Espíritu, Cristo hace portentos para liberar al hombre de sus miserias. El mismo Jesús dijo: "Si echo los demonios con el Espíritu de Dios, es señal de que ha llegado a vosotros el reino de Dios" (Mt 12,28). Después de la ascensión de Jesús a los cielos, es el Espíritu el que profundiza y difunde el mensaje de Cristo. El nos hace acoger con fe y con amor a la persona del Hijo y nos enseña a rezar: ¡Abba, Padre nuestro!

Hay cuatro lugares privilegiados de revelación del Espíritu Santo. El primero es la virgen María. El moró en ella. La elevó a la altura de lo divino. Por eso lo que nace de María, como dice san Lucas, será llamado Hijo de Dios (Lc 1,35). Lo femenino fue tocado por lo divino y también eternizado. La mujer posee en Dios su propio lugar.

El segundo lugar es Jesucristo. Jesús estaba lleno del Espíritu. Por eso era el hombre nuevo, totalmente libre y liberado de todas las ataduras históricas. En la fuerza del Espíritu lanza su programa mesiánico de total liberación (Lc 4,18-21). El Espíritu y Cristo siempre estarán juntos para conducir de nuevo a la creación al seno de la santísima Trinidad.

El tercer lugar es la misión. El Espíritu baja en pentecostés sobre los apóstoles, les quita el miedo y los envía a difundir el mensaje de Cristo entre todos los pueblos. Es el Espíritu el que en la misión permite ver y realizar la unidad en la pluralidad de naciones y de lenguas. La variedad no tiene por qué significar confusión, sino riqueza de la unidad.

El cuarto lugar es la comunidad humana y eclesial. Dentro de ella aparecen muchos servicios y habilidades. Unos saben consolar, otros coordinar, otros escribir, otros construir. De la misma forma, en la comunidad cristiana existe todo tipo de servicios y ministerios, bien en favor de la comunidad o bien en favor de la sociedad, rompiendo muchas veces los esquemas e inaugurando lo nuevo. Todo proviene del Espíritu. Los cristianos han meditado sobre estas manifestaciones y han sacado la siguiente conclusión: el Espíritu Santo también es Dios con el Padre y el Hijo. No son tres dioses, sino un solo Dios en comunión de personas.

> Estas son las señales de la presencia del Espíritu: cuando hay entusiasmo en el trabajo de la comunidad, cuando hay coraje para inventar caminos nuevos para nuevos problemas, cuando hay resistencia contra todo género de opresión, cuando hay voluntad de liberación empezando por la justicia de los pobres, cuando hay hambre y sed de Dios y unción en el corazón.


## 13. La conciencia trinitaria de los primeros cristianos

En el Nuevo Testamento tenemos la revelación de la santísima Trinidad. Pero no existe allí una doctrina elaborada sobre este hecho. La doctrina supone el cuestionamiento, la reflexión y la sistematización de las ideas. Esto no surgirá hasta dos siglos más tarde, cuando los cristianos tuvieron que elaborar ideas claras sobre la divinidad de Jesús y la del Espíritu Santo.

Pero en los escritos de los primeros cristianos, particularmente en las cartas de san Pablo, de san Pedro y de san Juan, se percibe la conciencia trinitaria. Esta conciencia se expresa mediante fórmulas ternarias, es decir, mediante formas de pensar y de hablar en las que el Padre, el Hijo y el Espíritu Santo aparecen siempre juntos. Este hecho demuestra que hay allí una fe en la santísima Trinidad, aunque no se perciba claramente una doctrina bien elaborada sobre la misma; podemos decir que esta doctrina sólo está allí a manera de embrión. Veamos algunos de los textos más significativos.

El primero es el de la comunidad eclesial de san Mateo: "Id, pues, y haced discípulos míos en todos los pueblos, bautizándolos en el nombre del Padre y del Hijo y del Espíritu Santo" (28,19). Ya hemos dicho que se trata de un texto tardío (por el año 85) y significa que por el bautismo el fiel es introducido en la comunidad de la Trinidad y está bajo la protección de los divinos tres.

El segundo texto en importancia es el de san Pablo, que hoy se utiliza en todas las misas: "La gracia de Jesucristo, el Señor; el amor de Dios y la comunión del Espíritu Santo estén con todos vosotros" (2Cor 13,13). La fórmula ternaria es tan explícita que nos dispensa de todo comentario.

Otro texto trinitario es el de la carta a los Tesalonicenses: "Pero nosotros debemos dar continuamente gracias a Dios por vosotros, hermanos queridos del Señor, porque Dios os ha escogido desde el principio para salvaros por la acción santificadora del Espíritu y la fe en la verdad. Precisamente para esto os llamó por nuestra predicación del evangelio, para que alcancéis la gloria de nuestro Señor Jesucristo" (2Tes 2,13-14). Aquí aparecen juntos, en la obra de la salvación, los divinos tres. Conviene recordar que siempre que el Nuevo Testamento habla de Dios sobrentiende al Padre. Textos semejantes a los citados son los de 1 Cor 12,4-6 y Gál 3,11-14; 2Cor 1,21-22; 3,3; Rom 14,17-18; 15,16; 15,40; Flp 3,3; Ef 2,20-22; 3,14-16.

Destaquemos, además, otros textos en virtud de su claridad: "Y como prueba de que sois hijos, Dios ha enviado a vuestroscorazones el Espíritu de su Hijo, que clama: ¡Abba, Padre! (Gál 4,6). "Dios es el que a nosotros y a vosotros nos mantiene firmes en Cristo y nos ha consagrado; él nos ha marcado con su sello y ha puesto en nuestros corazones el Espíritu como prenda de salvación" (2Cor 1,21-22). "Por él (por Jesucristo) los unos y los otros tenemos acceso al Padre en un mismo Espíritu" (Ef 2,18).

Habría otros textos que podrían ser leídos sin mayores explicaciones, como en la epístola de Tito 3,4-6, en la primera de Pedro 1,2, en la epístola de Judas 20-21, en el Apocalipsis 1,4.5 y en otros más.

La tónica de estos textos es siempre la siguiente: en la obra de la aproximación liberadora de Dios a los seres humanos siempre aparecen los tres divinos en comunión, actuando juntos e insertándonos en su vida divina.

> Más importante que la conciencia del bien es hacer el bien. Más importante que saber cómo el Padre, el Hijo y el Espíritu Santo son un solo Dios es vivir la comunión, que es la esencia de la Trinidad.


## 14. El Antiguo Testamento: Preparación para la revelación de la santísima Trinidad

Si el único Dios verdadero se llama Trinidad de personas, Padre, Hijo y Espíritu Santo, entonces hemos de admitir que toda revelación divina, en cualquier parte de la historia, significa una manifestación de la santísima Trinidad. Ciertamente, la gente no sabe que el encuentro con Dios implica siempre un encuentro con las tres divinas personas; pero una vez descubierta esta verdad, siempre podemos decir: toda experiencia auténtica de Dios significa realmente una experiencia del Dios trinitario. A la luz de esta verdad podemos releer las religiones del mundo, y particularmente el Antiguo Testamento. Allí percibimos indicios de una conciencia de que en Dios hay diversidad y de que en él existe la comunión y el amor. Así, en el Antiguo Testamento se profesa la fe de que existe solamente un único dios, pero al mismo tiempo se afirma que este Dios salió de sí mismo, que estableció una alianza con los hombres y con las mujeres, que toma partido por los oprimidos y quiere su liberación.

En los escritos del Antiguo Testamento descubrimos tres personificaciones que aluden a la fe futura en la santísima Trinidad. En primer lugar, se personifica la sabiduría. Ella es el Dios presente entre los hombres, que abre caminos donde hay dudas, que enciende la luz en medio de la búsqueda de los hombres. Ella es Dios, pero posee una relativa autonomía respecto al mismo Dios. En segundo lugar, se personifica la palabra de Dios. Por la palabra, Dios está en medio de la comunidad; por medio de ella él comunica su voluntad, juzga la historia, salva y promete al futuro liberador. Esta palabra es Dios, pero al mismo tiempo mantiene una relativa independencia de él, lo cual demuestra que en Dios hay unidad y diversidad. Finalmente, se personifica también a la fuerza de Dios: es el Espíritu de sabiduría, de discernimiento, de coraje, de santidad. Esta fuerza de Dios se manifiesta en la creación, en la historia, en la vida de las personas, particularmente en los justos y en los profetas. El Nuevo Testamento vio en estas manifestaciones la presencia del Espíritu Santo, tercera persona de la santísima Trinidad.

La santísima Trinidad quiso manifestarse progresivamente a las personas humanas. Primero, como enseñaba san Epifanio, "se enseña la unidad en Moisés, luego se anuncia la dualidad en los profetas y, finalmente, se encuentra la Trinidad en los evangelios".

>La revelación es como la vida. Hay siempre una preparación de lo que va a surgir. La aurora prepara el sol naciente, la semilla la planta, la flor el fruto. Así, el Antiguo Testamento prepara el Nuevo; el Dios de la alianza, al Dios de la comunión.
